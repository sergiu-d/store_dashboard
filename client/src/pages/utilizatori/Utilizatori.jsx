import React, { useState } from "react";

// Components //
import PageTitle from "../../components/PageTitle";
import Form from "../../components/form/Form";

const Utilizatori = () => {
  const [values, setValues] = useState({
    username: "",
    first_name: "",
    last_name: "",
    email: "",
    phone: null,
    location: "",
    user_role: "",
    password: "",
    confirmPassword: "",
  });

  const handleSubmit = (el) => {
    el.preventDefault();
    console.log(values);
  };

  // INPUT [name = str, label = str, type=(input, select, autocomplete) , disabled = boolean, options = array]
  const inputs = [
    {
      name: "username",
      label: "Utilizator",
      disabled: false,
      type: "text",
      variant: "input",
    },

    {
      name: "first_name",
      label: "Prenume",
      disabled: false,
      type: "text",
      variant: "input",
    },
    {
      name: "last_name",
      label: "Nume",
      disabled: false,
      type: "text",
      variant: "input",
    },
    {
      name: "email",
      label: "Email",
      disabled: false,
      type: "email",
      variant: "input",
    },
    {
      name: "phone",
      label: "Telefon",
      disabled: false,
      type: "number",
      variant: "input",
    },
    {
      name: "password",
      label: "Parola",
      disabled: false,
      type: "text",
      variant: "input",
    },
    {
      name: "passwordConfirm",
      label: "Repeta parola",
      disabled: false,
      type: "text",
      variant: "input",
    },
  ];

  return (
    <>
      <PageTitle title={"Adauga utilizator"} />
      <Form
        inputs={inputs}
        handleSubmit={handleSubmit}
        state={{ values, setValues }}
      />
    </>
  );
};

export default Utilizatori;
