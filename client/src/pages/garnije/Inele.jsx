import React, { useState } from "react";

// Components //
import PageTitle from "../../components/PageTitle";
import Form from "../../components/form/Form";

const Inele = () => {
  const [values, setValues] = useState({
    dimension: null,
    color: "",
    shape: "",
    price: "",
  });

  const handleSubmit = (el) => {
    el.preventDefault();
    console.log(values);
  };

  // INPUT [name = str, label = str, type=(input, select, autocomplete) , disabled = boolean, options = array]
  const inputs = [
    {
      name: "dimension",
      label: "Marime",
      disabled: false,
      type: "number",
      variant: "autocomplete",
      options: [16, 19, 25, 30],
    },

    {
      name: "color",
      label: "Culoare",
      disabled: false,
      type: "text",
      variant: "input",
    },
    {
      name: "shape",
      label: "Forma",
      disabled: false,
      type: "text",
      variant: "select",
      options: ["rotund", "patrat"],
    },
    {
      name: "price",
      label: "Pret",
      disabled: false,
      type: "text",
      variant: "input",
    },
  ];
  return (
    <>
      <PageTitle title={"Adauga inele"} />
      <Form
        inputs={inputs}
        handleSubmit={handleSubmit}
        state={{ values, setValues }}
      />
    </>
  );
};

export default Inele;
