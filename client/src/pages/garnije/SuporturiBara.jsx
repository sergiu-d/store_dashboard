import React, { useState, useEffect } from "react";

// Components //
import PageTitle from "../../components/PageTitle";
import Form from "../../components/form/Form";

const SuporturiBara = () => {
  const [values, setValues] = useState({
    collection: "",
    type: "",
    shape: "",
    width: "",
    price: null,
  });

  const [widthOptions, setWidthOptions] = useState({
    name: "width",
    label: "Marime",
    disabled: true,
    type: "text",
    variant: "autocomplete",
    options: [],
  });

  const handleSubmit = (el) => {
    el.preventDefault();
    console.log(values);
  };

  const getWidthOptions = () => {
    if (values.collection && values.type && values.shape) {
      // If Wall is selected
      if (values.collection === "perete") {
        if (values.type === "simplu" && values.shape === "rotund")
          return setWidthOptions((prev) => ({
            ...prev,
            disabled: false,
            options: ["19", "25", "30"],
          }));
        if (values.type === "simplu" && values.shape === "patrat")
          return setWidthOptions((prev) => ({
            ...prev,
            disabled: false,
            options: ["20"],
          }));

        if (values.type === "dublu" && values.shape === "rotund")
          return setWidthOptions((prev) => ({
            ...prev,
            disabled: false,
            options: [("19/19", "19/25", "16/16")],
          }));

        if (values.type === "dublu" && values.shape === "patrat")
          return setWidthOptions((prev) => ({
            ...prev,
            disabled: false,
            options: ["20/20"],
          }));
      }

      //If celling is selected
      if (values.collection === "tavan") {
        if (values.type === "simplu" && values.shape === "rotund")
          return setWidthOptions((prev) => ({
            ...prev,
            disabled: false,
            options: ["19", "25", "30"],
          }));

        if (values.type === "simplu" && values.shape === "patrat")
          return setWidthOptions((prev) => ({
            ...prev,
            disabled: false,
            options: ["20"],
          }));

        if (values.type === "dublu" && values.shape === "rotund")
          return setWidthOptions((prev) => ({
            ...prev,
            disabled: false,
            options: ["19/19", "19/25", "16/16", "19/30"],
          }));

        if (values.type === "dublu" && values.shape === "patrat")
          return setWidthOptions((prev) => ({
            ...prev,
            disabled: false,
            options: ["20/20"],
          }));
      }
    }
  };

  useEffect(() => {
    getWidthOptions();
  }, [values.collection, values.type, values.shape]);

  // INPUT [name = str, label = str, type=(input, select, autocomplete) , disabled = boolean, options = array]
  const inputs = [
    {
      name: "collection",
      label: "Gama",
      disabled: false,
      type: "text",
      variant: "select",
      options: ["perete", "tavan"],
    },
    {
      name: "type",
      label: "Tip",
      disabled: false,
      type: "text",
      variant: "select",
      options: ["simplu", "dublu"],
    },
    {
      name: "shape",
      label: "Forma",
      disabled: false,
      type: "text",
      variant: "select",
      options: ["rotund", "patrat"],
    },
    {
      ...widthOptions,
    },
    {
      name: "price",
      label: "Pret",
      disabled: false,
      type: "text",
      variant: "input",
    },
  ];
  return (
    <>
      <PageTitle title={"Suporturi Bara"} />
      <Form
        inputs={inputs}
        handleSubmit={handleSubmit}
        state={{ values, setValues }}
      />
    </>
  );
};

export default SuporturiBara;
