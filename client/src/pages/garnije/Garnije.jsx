import React, { useState } from "react";

// Components //
import PageTitle from "../../components/PageTitle";
import Form from "../../components/form/Form";

const Garnije = () => {
  const [values, setValues] = useState({
    collection: "",
    color: "",
    thickness: null,
    shape: "",
    width: "",
    price: null,
  });

  const handleSubmit = (el) => {
    el.preventDefault();
    console.log(values);
  };

  // INPUT [name = str, label = str, type=(input, select, autocomplete) , disabled = boolean, options = array]
  const inputs = [
    {
      name: "collection",
      label: "Gama",
      disabled: false,
      type: "text",
      variant: "input",
    },
    {
      name: "color",
      label: "Culoare",
      disabled: false,
      type: "input",
      variant: "input",
    },
    {
      name: "thickness",
      label: "Grosime",
      disabled: false,
      type: "number",
      variant: "autocomplete",
      options: ["16", "19", "20", "25", "30"],
    },
    {
      name: "shape",
      label: "Forma",
      disabled: false,
      type: "text",
      variant: "select",
      options: ["rotund", "patrat", "nespecificat"],
    },
    {
      name: "width",
      label: "Lungime",
      disabled: false,
      type: "text",
      variant: "autocomplete",
      options: ["1.6", "2.0", "2.4", "3.0"],
    },
    {
      name: "price",
      label: "Pret",
      disabled: false,
      type: "number",
      variant: "input",
    },
  ];
  return (
    <>
      <PageTitle title={"Adauga Garnije"} />
      <Form
        inputs={inputs}
        handleSubmit={handleSubmit}
        state={{ values, setValues }}
      />
    </>
  );
};

export default Garnije;
