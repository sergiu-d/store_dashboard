import React, { useState, useEffect } from "react";

// Components //
import PageTitle from "../../components/PageTitle";
import Form from "../../components/form/Form";

const AccUnghiulare = () => {
  const [values, setValues] = useState({
    shape: "",
    width: "",
    color: "",
    price: null,
  });

  const [widthOptions, setWidthOptions] = useState({
    name: "width",
    label: "Marime",
    disabled: true,
    type: "number",
    variant: "autocomplete",
    options: [],
  });

  const handleSubmit = (el) => {
    el.preventDefault();
    console.log(values);
  };

  const getWidthOptions = () => {
    if (values.shape === "rotunde")
      return setWidthOptions((prev) => ({
        ...prev,
        disabled: false,
        options: [16, 19, 25, 30],
      }));

    if (values.shape === "patrate")
      return setWidthOptions((prev) => ({
        ...prev,
        disabled: false,
        options: [20],
      }));
  };

  // INPUT [name = str, label = str, type=(input, select, autocomplete) , disabled = boolean, options = array]
  const inputs = [
    {
      name: "shape",
      label: "Forma",
      disabled: false,
      type: "text",
      variant: "select",
      options: ["rotunde", "patrate"],
    },
    {
      ...widthOptions,
    },

    {
      name: "price",
      label: "Pret",
      disabled: false,
      type: "text",
      variant: "input",
    },
  ];

  useEffect(() => {
    setWidthOptions((prev) => ({
      ...prev,
      options: [],
    }));
    getWidthOptions();
  }, [values.shape]);
  return (
    <>
      <PageTitle title={"Adauga accesorii unghiulare"} />
      <Form
        inputs={inputs}
        handleSubmit={handleSubmit}
        state={{ values, setValues }}
      />
    </>
  );
};

export default AccUnghiulare;
