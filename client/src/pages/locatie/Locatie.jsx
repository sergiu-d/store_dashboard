import React, { useState } from "react";

// Components //
import PageTitle from "../../components/PageTitle";
import Form from "../../components/form/Form";

const Locatie = () => {
  const [values, setValues] = useState({
    city: "",
    number: null,
  });

  const handleSubmit = (el) => {
    el.preventDefault();
    console.log(values);
  };

  // INPUT [name = str, label = str, type=(input, select, autocomplete) , disabled = boolean, options = array]
  const inputs = [
    {
      name: "city",
      label: "Oras",
      disabled: false,
      type: "text",
      variant: "input",
    },

    {
      name: "number",
      label: "Numar magazin",
      disabled: false,
      type: "number",
      variant: "input",
    },
  ];

  return (
    <>
      <PageTitle title={"Adauga locatie"} />
      <Form
        inputs={inputs}
        handleSubmit={handleSubmit}
        state={{ values, setValues }}
      />
    </>
  );
};

export default Locatie;
