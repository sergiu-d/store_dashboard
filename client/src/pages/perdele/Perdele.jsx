import React, { useState } from "react";

// Components //
import PageTitle from "../../components/PageTitle";
import Form from "../../components/form/Form";

const Perdele = () => {
  const [values, setValues] = useState({
    name: "",
    code: "",
    variant: "",
    color: "",
    price: null,
  });

  const handleSubmit = (el) => {
    el.preventDefault();
    console.log(values);
  };

  // INPUT [name = str, label = str, type=(input, select, autocomplete) , disabled = boolean, options = array]
  const inputs = [
    {
      name: "name",
      label: "Nume",
      disabled: false,
      type: "text",
      variant: "input",
    },

    {
      name: "code",
      label: "Cod",
      disabled: false,
      type: "text",
      variant: "input",
    },
    {
      name: "variant",
      label: "Varianta",
      disabled: false,
      type: "text",
      variant: "input",
    },
    {
      name: "color",
      label: "Culoare",
      disabled: false,
      type: "text",
      variant: "input",
    },
    {
      name: "price",
      label: "Pret",
      disabled: false,
      type: "text",
      variant: "input",
    },
  ];

  return (
    <>
      <PageTitle title={"Adauga perdea"} />
      <Form
        inputs={inputs}
        handleSubmit={handleSubmit}
        state={{ values, setValues }}
      />
    </>
  );
};

export default Perdele;
