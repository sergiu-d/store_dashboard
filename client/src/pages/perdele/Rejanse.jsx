import React, { useState } from "react";

// Components //
import PageTitle from "../../components/PageTitle";
import Form from "../../components/form/Form";

const Rejanse = () => {
  const [values, setValues] = useState({
    name: "",
    width: "",
    model: "",
    price: null,
  });

  const handleSubmit = (el) => {
    el.preventDefault();
    console.log(values);
  };

  // INPUT [name = str, label = str, type=(input, select, autocomplete) , disabled = boolean, options = array]
  const inputs = [
    {
      name: "name",
      label: "Nume",
      disabled: false,
      type: "text",
      variant: "select",
      options: ["mbai", "transparent"],
    },

    {
      name: "width",
      label: "Latime",
      disabled: false,
      type: "text",
      variant: "select",
      options: [6, 10, 2.5],
    },
    {
      name: "model",
      label: "Model",
      disabled: false,
      type: "text",
      variant: "input",
    },
    {
      name: "price",
      label: "Pret",
      disabled: false,
      type: "text",
      variant: "input",
    },
  ];

  return (
    <>
      <PageTitle title={"Adauga rejanse"} />
      <Form
        inputs={inputs}
        handleSubmit={handleSubmit}
        state={{ values, setValues }}
      />
    </>
  );
};

export default Rejanse;
