import React, { useState } from "react";

// Components //
import PageTitle from "../../components/PageTitle";
import Form from "../../components/form/Form";

const AccPerdele = () => {
  const [values, setValues] = useState({
    name: "",
    color: "",
    price: null,
  });

  const handleSubmit = (el) => {
    el.preventDefault();
    console.log(values);
  };

  // INPUT [name = str, label = str, type=(input, select, autocomplete) , disabled = boolean, options = array]
  const inputs = [
    {
      name: "name",
      label: "Denumire",
      disabled: false,
      type: "text",
      variant: "input",
    },

    {
      name: "color",
      label: "Culoare",
      disabled: false,
      type: "text",
      variant: "input",
    },

    {
      name: "price",
      label: "Pret",
      disabled: false,
      type: "text",
      variant: "input",
    },
  ];

  return (
    <>
      <PageTitle title={"Adauga accesorii perdele"} />
      <Form
        inputs={inputs}
        handleSubmit={handleSubmit}
        state={{ values, setValues }}
      />
    </>
  );
};

export default AccPerdele;
