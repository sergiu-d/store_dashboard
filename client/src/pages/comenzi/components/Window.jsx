import React, { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Products from "./Products";
import Stack from "@mui/material/Stack";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";

const Window = (props) => {
  const { ids, handleRemoveWindow, setGetWindowsState } = props;
  const { roomId, windowId } = ids;
  const [windowsState, setWindowsState] = React.useState({});

  useEffect(() => {
    setGetWindowsState((prev) => {
      return { ...prev, windowsState };
    });
  }, [windowsState]);

  console.log(
    "🚀 ~ file: Window.jsx ~ line 13 ~ Window ~ windowsState",
    windowsState
  );
  return (
    <Paper elevation={2} sx={{ padding: "1rem" }}>
      <Stack spacing={5}>
        <Button
          id={windowId}
          variant="outlined"
          color="error"
          size="small"
          sx={{
            minWidth: "fit-content",
            alignSelf: "flex-end",
            padding: ".2rem .8rem",
          }}
          onClick={(el) => handleRemoveWindow(el)}
        >
          X
        </Button>
        <TextField
          label="Nume geam"
          color="secondary"
          fullWidth
          onBlur={(el) =>
            setWindowsState((prev) => ({
              ...prev,
              window_name: el.target.value,
            }))
          }
        />
        <Box>
          <Products
            ids={ids}
            windowsState={windowsState}
            setWindowsState={setWindowsState}
          />
        </Box>
      </Stack>
    </Paper>
  );
};

export default Window;
