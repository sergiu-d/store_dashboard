import React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Box from "@mui/material/Box";

const Selector = (props) => {
  const { id, label, state, setState, options = [], style } = props;

  const handleChange = (event) => setState(event.target.value);
  return (
    <Box sx={style}>
      <FormControl fullWidth>
        <InputLabel id={`select-label${id}`}>{label}</InputLabel>
        <Select
          fullWidth
          labelId={id}
          label={label}
          value={state}
          onChange={handleChange}
          disabled={!options.length}
          variant={!options.length ? "filled" : "outlined"}
          sx={{
            textTransform: "capitalize",
          }}
        >
          <MenuItem value="">Nici unul</MenuItem>
          {options.map((option, index) => {
            return (
              <MenuItem
                key={index}
                value={option.value || option}
                sx={{
                  textTransform: "capitalize",
                }}
              >
                {option.label || option}
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </Box>
  );
};

export default Selector;
