import React, { useState, useContext, useEffect } from "react";
import OrderTable from "../tables/OrderTable";
import AccTable from "../tables/AccTable";

import Selector from "./Selector";

import { Stack, Box } from "@mui/material";

// Context
import { OrderContext } from "../../../context/OrderContext";
import { WindowContext } from "../../../context/OrderContext";

// import Fetcher
import { fetcher } from "../../../utils/fetcher";
import useSWR, { useSWRConfig } from "swr";

const options = {
  // To add more products and accessories, just use the same format as the ones below.
  // The KEYS are the VALUE and OPTIONS of the PRODUCT selector.
  // The VALUE property is used to FETCH the table data. It should be the same as the TABLE from DB.
  garnije: [
    { value: "acc_poles", label: "Suporturi bara" },
    { value: "acc_polesTemp", label: "Consola bara" },
    { value: "pole_sides", label: "Capete de bara" },
    { value: "pole_rings", label: "Inele" },
    { value: "acc_angular", label: "Accesorii ungiulare" },
  ],
  perdele: [
    { value: "curtains", label: "Perde" },
    { value: "grosgrains", label: "Rejanse" },
    { value: "acc_curtains", label: "Accesorii perdele" },
  ],
};

const Products = (props) => {
  const { ids, windowsState, setWindowsState } = props;
  const { roomId, windowId } = ids;

  const [productValue, setProductValue] = useState("");

  const [accValue, setAccValue] = useState("");

  const { data, error } = useSWR(accValue ? `api/${accValue}` : null, fetcher);
  console.log("🚀 ~ file: Products.jsx ~ line 44 ~ Products ~ data", data);

  const productOptions = Object.keys(options);
  const accOptions = options[productValue];

  const handleAddWindowAcc = (acc) => {
    setWindowsState((prev) => {
      // console.log(
      //   "🚀 ~ file: Products.jsx ~ line 54 ~ setWindowsState ~ prev",
      //   prev
      // );
      // return {
      //   ...prev,
      //   [productValue]: {
      //     ...prev[productValue],
      //     [accValue]: [acc],
      //   },
      // };

      // console.log(
      //   "🚀 ~ file: Products.jsx ~ line 53 ~ setWindowsState ~  prev[productValue]",
      //   prev[productValue]
      // );
      // const prevProductValue = prev[productValue];
      // console.log(
      //   "🚀 ~ file: Products.jsx ~ line 53 ~ setWindowsState ~ prevProductValue",
      //   prevProductValue
      // );
      // const prevAccessoryValue = prevProductValue[accValue];

      // return { ...prev, name: "wre" };

      if (!Object.keys(prev).length) {
        return { [productValue]: { [accValue]: [acc] } };
      } else {
        // const filterDuplicates = () =>
        //   prev[productValue][accValue].filter(
        //     (prevAcc) => prevAcc.id !== acc.id
        //   );

        // const checkAccValue = () =>
        //   !prev[productValue][accValue]
        //     ? [acc]
        //     : [...prev[productValue], ...filterDuplicates()];

        // const checkProductValue = () =>
        //   !prev[productValue]
        //     ? { [accValue]: [acc] }
        //     : { ...prev[productValue], [accValue]: checkAccValue() };

        // return {
        //   ...prev,
        //   [productValue]: checkProductValue(),
        // };

        if (!Object.keys(prev).includes(productValue)) {
          return { ...prev, [productValue]: { [accValue]: [acc] } };
        } else {
          const checkAccValue = Object.keys(prev[productValue]).includes(
            accValue
          )
            ? [...prev[productValue][accValue], acc]
            : [acc];

          return {
            ...prev,
            [productValue]: {
              ...prev[productValue],
              [accValue]: checkAccValue,
            },
          };
        }
      }
    });
  };

  return (
    <Stack spacing={8}>
      <OrderTable />
      <OrderTable />
      <Stack direction={"row"} spacing={5}>
        {/* Product Selector */}
        <Selector
          id="productSelector"
          label="Produs"
          state={productValue}
          setState={setProductValue}
          options={productOptions}
          style={{ flexGrow: 1 }}
        />
        {/* Accessories Selector */}
        <Selector
          id="accSelector"
          label="Accesoriu"
          state={accValue}
          setState={setAccValue}
          options={accOptions}
          style={{ flexGrow: 3 }}
        />
      </Stack>

      {data && (
        <Box>
          <AccTable
            data={data}
            error={error}
            accType={accValue}
            handleAddWindowAcc={handleAddWindowAcc}
          />
        </Box>
      )}
    </Stack>
  );
};

export default Products;
