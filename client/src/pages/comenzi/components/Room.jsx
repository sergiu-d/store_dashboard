import React, { useState, useRef, useContext, useEffect } from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import Paper from "@mui/material/Paper";

import Window from "./Window";

import Button from "@mui/material/Button";

// Context
import { OrderContext } from "../../../context/OrderContext";

const Room = (props) => {
  const { id: roomId, handleRemoveRoom, roomsObj, setRoomsObj } = props;

  const [windows, setWindows] = useState([]);

  const windowKey = useRef(0);
  const [getWindowsState, setGetWindowsState] = useState({});

  const handleRemoveWindow = (el) =>
    setWindows((prev) => prev.filter((item) => +item.key !== +el.target.id));

  const handleAddWindow = () => {
    windowKey.current = windowKey.current + 1;

    setWindows((prev) => [
      ...prev,
      <Window
        key={windowKey.current}
        id={windowKey.current}
        ids={{ roomId, windowId: windowKey.current }}
        handleRemoveWindow={handleRemoveWindow}
        setGetWindowsState={setGetWindowsState}
      />,
    ]);

    setRoomsObj((prev) => {
      const findRoomId = prev.find((item) => item.id === roomId);
      findRoomId.windows.push({
        id: windowKey.current,
        window_name: "",
        ...getWindowsState,
      });

      const uniqueRoomObjArray = [
        ...new Map(
          [...prev, findRoomId].map((item) => [item["id"], item])
        ).values(),
      ];

      return uniqueRoomObjArray;
    });
  };

  return (
    <Paper elevation={4} sx={{ padding: "1rem" }}>
      <Stack direction={"column"} spacing={5}>
        <Button
          id={roomId}
          variant="contained"
          color="error"
          size="small"
          sx={{
            minWidth: "fit-content",
            alignSelf: "flex-end",
            padding: ".2rem .8rem",
          }}
          onClick={handleRemoveRoom}
        >
          X
        </Button>
        <TextField label="Nume camera" color="secondary" />
        <Box
          style={{
            display: "flex",
            justifyContent: "center",
            gap: "8rem",
            position: "sticky",
            top: "68px",
            zIndex: "10000000",
            backgroundColor: "white",
          }}
        >
          {/* Add window */}
          <Button
            variant="outlined"
            sx={{ color: "#0097a7" }}
            onClick={handleAddWindow}
          >
            Adauga geam
          </Button>
          <Button variant="outlined" sx={{ visibility: "hidden" }}>
            Adauga geam
          </Button>
        </Box>
        <Stack spacing={5}>{windows.map((window) => window)}</Stack>
      </Stack>
    </Paper>
  );
};
export default Room;
