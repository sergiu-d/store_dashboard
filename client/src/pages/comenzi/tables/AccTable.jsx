import React, { useContext } from "react";
import { Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

// Context
// import { WindowContext } from "../../context/OrderContext";

// use context

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

const tableHeader = {
  poles: ["Gama", "Culoare", "Grosime", "Forma", "Lungime", "Pret"],
  acc_poles: ["Gama", "Tip", "Forma", "Lungime", "Pret"],
  acc_polesTemp: ["Denumire", "Culoare", "Pret"],
  pole_sides: ["Nume", "Dimensiune", "Forma", "Culoare", "Pret"],
  pole_rings: ["Dimensiune", "Culoare", "Forma", "Pret"],
  acc_angular: ["Forma", "Marime", "Culoare"],
  curtains: ["Nume", "Cod", "Varianta", "Culoare", "Pret"],
  grosgrains: ["Nume", "Dimensiune", "Model", "Pret"],
  acc_curtains: ["Nume", "Culoare", "Pret"],
};

const AccTable = (props) => {
  const { data, error, accType, handleAddWindowAcc } = props;

  if (!data) return <div>Loading...</div>;
  if (error) return <div>Error</div>;
  return (
    <TableContainer component={Paper}>
      <Table aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="center">#</StyledTableCell>
            {tableHeader[accType].map((item, index) => (
              <StyledTableCell align="center" key={index}>
                {item}
              </StyledTableCell>
            ))}

            <StyledTableCell align="center">Actiuni</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row, index) => (
            <StyledTableRow key={index}>
              <StyledTableCell component="th" scope="row" align="center">
                {index}
              </StyledTableCell>
              {Object.keys(row).map((key, index) => {
                if (key === "id") return;
                return (
                  <StyledTableCell align="center" key={index}>
                    {row[key]}
                  </StyledTableCell>
                );
              })}

              <StyledTableCell align="center">
                <button id={row.id} onClick={() => handleAddWindowAcc(row)}>
                  Adauga
                </button>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default AccTable;
