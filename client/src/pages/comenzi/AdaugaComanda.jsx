import React, { useContext, useEffect } from "react";

// Components
import Container from "../../components/Container";

// Material-UI //
import {
  Typography,
  Paper,
  Box,
  Button,
  Stack,
  TextField,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import DateAdapter from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import DatePicker from "@mui/lab/DatePicker";

import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

//Context

const useStyles = makeStyles((theme) => ({
  phoneInput: {
    "& .special-label": {
      display: "block",
    },
  },
}));

const emptyWindow = {
  name: "",
  poles: [],
  curtains: [],
};
const emptyRoom = {
  name: "",
  windows: [],
};

const emptyOrder = {
  client_name: "",
  phone: "",
  fulfillment_date: null,
  products: [],
  rooms: [{ ...emptyRoom, name: "living" }],
};
// objectPath.set(emptyOrder, 'rooms.0.windows.0', );

const Room = ({ index }) => {
  const { order, setOrder } = useContext(OrderCtx);
  const room = order.rooms[index];
  const addWindow = (name) => {
    room.windows.push({ ...emptyWindow, name });
    setOrder({
      ...order,
    });
  };
  return (
    <>
      <button onClick={() => addWindow("a window")}>Add Window</button>
      <pre>{JSON.stringify(room, 2, 2)}</pre>;
    </>
  );
};

const OrderCtx = React.createContext();

const OrderCtxProvider = ({ children }) => {
  const [order, setOrder] = React.useState(emptyOrder);
  const value = { order, setOrder };
  return <OrderCtx.Provider value={value}>{children}</OrderCtx.Provider>;
};

const AdaugaComanda = () => {
  const { order, setOrder } = useContext(OrderCtx);
  console.log("ORDER", order);
  const rooms = order.rooms.map((r, i) => <Room index={i} />);
  const addRoom = (name) => {
    setOrder({
      ...order,
      rooms: [...order.rooms, { ...emptyRoom, name }],
    });
  };
  return (
    <>
      <div>
        <button onClick={() => addRoom("another room")}>Add Room</button>
      </div>
      {rooms}
    </>
  );
};

export default () => (
  <OrderCtxProvider>
    <AdaugaComanda />
  </OrderCtxProvider>
);

const _AdaugaComanda = () => {
  const classes = useStyles();
  const [value, setValue] = React.useState(new Date());
  const [phone, setPhone] = React.useState({});
  const [roomsObj, setRoomsObj] = React.useState([]);

  const [room, setRoom] = React.useState([]);
  const roomKey = React.useRef(0);

  const handleRemoveRoom = (el) => {
    const elementId = +el.target.id;

    setRoom((prev) => prev.filter((item) => +item.key !== elementId));
    setRoomsObj((prev) => prev.filter((item) => item.id !== elementId));
  };

  const handlePhoneNumberChange = (value) =>
    setPhone({
      phone: value,
    });

  const handleAddRoom = () => {
    roomKey.current = roomKey.current + 1;

    setRoom((prev) => [
      ...prev,
      <Room
        key={roomKey.current}
        id={roomKey.current}
        handleRemoveRoom={handleRemoveRoom}
        roomsObj={roomsObj}
        setRoomsObj={setRoomsObj}
      />,
    ]);

    setRoomsObj((prev) => {
      if (roomKey.current === 0) return [];

      const uniqueRoomObjArray = [
        ...new Map(prev.map((item) => [item["id"], item])).values(),
      ];

      return [
        ...uniqueRoomObjArray,
        { id: roomKey.current, room_name: "", windows: [] },
      ];
    });
  };

  return (
    <Container>
      <Paper
        elevation={2}
        sx={{
          padding: "1rem",
        }}
      >
        <Box>
          <Typography variant="h4">Detalii client</Typography>
        </Box>
        <Stack direction="column" justifyContent="center" spacing={2}>
          <TextField
            label="Nume client"
            color="secondary"
            name="client-name"
            type="text"
          />
          <PhoneInput
            containerClass={classes.phoneInput}
            inputProps={{
              name: "Numar telefon",
              required: true,
            }}
            containerStyle={{ display: "block" }}
            specialLabel="Numar telefon"
            alwaysDefaultMask
            regions={"europe"}
            enableSearch
            disableSearchIcon
            searchStyle={{ margin: "0", display: "block" }}
            inputStyle={{
              width: "100%",
              height: "55px",
              paddingTop: "1rem",
              paddingBottom: "1rem",
            }}
            value={phone.number}
            onChange={handlePhoneNumberChange}
          />
          <LocalizationProvider dateAdapter={DateAdapter}>
            <DatePicker
              openTo="year"
              views={["year", "month", "day"]}
              label="Data finalizare"
              value={value}
              onChange={(newValue) => {
                setValue(newValue);
              }}
              renderInput={(params) => (
                <TextField {...params} helperText={null} />
              )}
            />
          </LocalizationProvider>
        </Stack>
      </Paper>
      <Paper elevation={2} sx={{ padding: "1rem" }}>
        <Box>
          <Typography variant="h4">Produse</Typography>
        </Box>
        <Stack
          direction="row"
          justifyContent="center"
          spacing={10}
          sx={{
            position: "sticky",
            top: "0",
            zIndex: "100",
            padding: "1rem",
            background: "#fff",
          }}
        >
          <Button
            variant="outlined"
            onClick={handleAddRoom}
            href={`#${roomKey.current}`}
          >
            Adauga camera
          </Button>
          <Button variant="outlined">Adauga produse</Button>
        </Stack>
        <Stack spacing={8}>{room.map((room) => room)}</Stack>
      </Paper>
    </Container>
  );
};
