import React, { createContext, useState } from "react";

const OrderContext = createContext();
const RoomContext = createContext();
const WindowContext = createContext();

const OrderProvider = ({ children }) => {
  const [orderState, setOrderState] = useState({
    client_name: "",
    phone: null,
    fulfillment_date: "",
    order: {
      rooms: [],
      products: [],
    },
    up_front_payment: null,
    comment: "",
    discount: null,
    payment: null,
    location: null,
    status: null,
  });
  const [roomState, setRoomState] = useState({});
  const [windowState, setWindowState] = useState({});
  return (
    <OrderContext.Provider value={{ orderState, setOrderState }}>
      <RoomContext.Provider value={{ roomState, setRoomState }}>
        <WindowContext.Provider value={{ windowState, setWindowState }}>
          {children}
        </WindowContext.Provider>
      </RoomContext.Provider>
    </OrderContext.Provider>
  );
};

export { OrderContext, RoomContext, WindowContext, OrderProvider };
