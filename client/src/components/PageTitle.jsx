import React from "react";

// Material UI //
import { Typography, Box } from "@mui/material";

const PageTitle = ({ title }) => {
  return (
    <Box sx={{ padding: "0 0", marginBottom: "3rem" }}>
      <Typography variant="h4" sx={{ fontWeight: 400 }}>
        {title}
      </Typography>
    </Box>
  );
};

export default PageTitle;
