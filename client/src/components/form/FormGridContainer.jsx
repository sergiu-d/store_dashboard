import React from "react";

// Material UI //
import { Grid, Button } from "@mui/material";

function FormGridContainer({ children, handleSubmit }) {
  return (
    <form onSubmit={handleSubmit}>
      <Grid
        container
        rowSpacing={2}
        direction="row"
        justifyContent="center"
        alignItems="center"
      >
        {children}
        <Grid item xs={4}></Grid>
        <Grid item xs={8}>
          <Button
            type="submit"
            variant="contained"
            sx={{
              textTransform: "capitalize",
              fontWeight: 400,
              marginTop: "1.5rem",
            }}
          >
            Adauga
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}

export default FormGridContainer;
