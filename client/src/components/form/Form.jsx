import React from "react";

// Components //
import Container from "../../components/Container";
import FormGridContainer from "../../components/form/FormGridContainer";
import {
  LabeledInput,
  LabeledSelectInput,
  LabeledAutocompleteInput,
} from "../../components/form/InputsGridItem";

// Material UI //

const Form = (props) => {
  const { state, inputs, handleSubmit } = props;
  const { values, setValues } = state;

  const handelOnInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    setValues((prev) => ({ ...prev, [name]: value }));
  };

  return (
    <Container>
      <FormGridContainer handleSubmit={handleSubmit}>
        {inputs.map((input, index) => {
          if (input.variant === "input")
            return (
              <LabeledInput
                key={index}
                input={input}
                handelOnInput={handelOnInput}
              />
            );
          if (input.variant === "select") {
            return (
              <LabeledSelectInput
                key={index}
                input={input}
                value={values[input.name]}
                handelOnInput={handelOnInput}
              />
            );
          }
          if (input.variant === "autocomplete") {
            return (
              <LabeledAutocompleteInput
                key={index}
                input={input}
                value={values[input.name]}
                handelOnInput={handelOnInput}
              />
            );
          }
        })}
      </FormGridContainer>
    </Container>
  );
};

export default Form;
