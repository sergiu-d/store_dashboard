import React from "react";

// Material UI //
import {
  Grid,
  MenuItem,
  TextField,
  InputLabel,
  Autocomplete,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";

export const LabeledInput = ({ input, handelOnInput }) => {
  const theme = useTheme();
  return (
    <>
      <Grid item xs={4}>
        <InputLabel
          htmlFor={input.name}
          style={{ color: theme.palette.text.primary, fontWeight: 600 }}
        >
          {input.label}{" "}
          <span style={{ color: theme.palette.error.main }}>*</span>
        </InputLabel>
      </Grid>
      <Grid item xs={8}>
        <TextField
          id={input.name}
          size="small"
          name={input.name}
          type={input.type}
          onChange={handelOnInput}
          disabled={input.disabled}
          variant={input.disabled ? "filled" : "outlined"}
          fullWidth
          required
        />
      </Grid>
    </>
  );
};

export const LabeledSelectInput = ({ input, value, handelOnInput }) => {
  const theme = useTheme();
  return (
    <>
      <Grid item xs={4}>
        <InputLabel
          htmlFor={input.name}
          style={{ color: theme.palette.text.primary, fontWeight: 600 }}
        >
          {input.label}{" "}
          <span style={{ color: theme.palette.error.main }}>*</span>
        </InputLabel>
      </Grid>
      <Grid item xs={8}>
        <TextField
          id={input.name}
          size="small"
          name={input.name}
          value={value}
          type={input.type}
          disabled={input.disabled}
          variant={input.disabled ? "filled" : "outlined"}
          select
          fullWidth
          onChange={handelOnInput}
        >
          {input.options.map((option, index) => (
            <MenuItem
              key={index}
              value={option}
              sx={{ textTransform: "capitalize" }}
            >
              {option}
            </MenuItem>
          ))}
        </TextField>
      </Grid>
    </>
  );
};

export const LabeledAutocompleteInput = ({ input, value, handelOnInput }) => {
  const theme = useTheme();

  return (
    <>
      <Grid item xs={4}>
        <InputLabel
          htmlFor={input.name}
          style={{ color: theme.palette.text.primary, fontWeight: 600 }}
        >
          {input.label}{" "}
          <span style={{ color: theme.palette.error.main }}>*</span>
        </InputLabel>
      </Grid>
      <Grid item xs={8}>
        <Autocomplete
          name={input.name}
          size="small"
          freeSolo
          value={value}
          type={input.type}
          disabled={input.disabled}
          onChange={(_, newValue) =>
            handelOnInput({ target: { name: input.name, value: +newValue } })
          }
          getOptionLabel={(option) => `${option}`}
          options={input.options}
          renderInput={(params) => {
            return (
              <TextField
                {...params}
                variant={input.disabled ? "filled" : "outlined"}
              />
            );
          }}
        />
      </Grid>
    </>
  );
};
