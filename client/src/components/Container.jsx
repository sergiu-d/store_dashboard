import React from "react";

// Material-UI //
import { Button, Stack, Box, Paper } from "@mui/material";

const Container = ({ children }) => {
  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <Paper elevation={10} sx={{ width: "60%", padding: "25px" }}>
        {children}
      </Paper>
    </Box>
  );
};

export default Container;
