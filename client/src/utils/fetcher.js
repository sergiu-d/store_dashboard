const URL = "http://localhost:4500";

export const fetcher = (...args) =>
  fetch(`${URL}/${args}`).then((res) => res.json());

// export const post = async (path, method, params) => {
//   await fetch(`${URL}/${path}`, {
//     method: method,
//     mode: "cors",
//     headers: {
//       "Content-Type": "application/json",
//     },
//     body: JSON.stringify(params),
//   })
//     .then(async (res) => {
//       const isJson = res.headers
//         .get("content-type")
//         ?.includes("application/json");
//       const data = isJson && (await res.json());

//       // check for error res
//       if (!res.ok) {
//         // get error message from body or default to res status
//         const error = (data && data.message) || res.status;
//         return Promise.reject(error);
//       }
//     })
//     .catch((err) => console.log("This is an error ", err));
// };
