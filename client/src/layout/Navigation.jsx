import React, { useEffect } from "react";

// Router //
import { Link, useLocation } from "react-router-dom";

// Material UI //
import {
  Accordion,
  AccordionDetails,
  Stack,
  Drawer,
  Typography,
} from "@mui/material";

// Icons
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import GroupOutlinedIcon from "@mui/icons-material/GroupOutlined";
import useMediaQuery from "@mui/material/useMediaQuery";
import ArrowRightOutlinedIcon from "@mui/icons-material/ArrowRightOutlined";
import BlurOnOutlinedIcon from "@mui/icons-material/BlurOnOutlined";
import AccordionSummary from "@mui/material/AccordionSummary";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ShoppingCartOutlinedIcon from "@mui/icons-material/ShoppingCartOutlined";

// Styles
import { makeStyles } from "@mui/styles";
import { useTheme } from "@mui/material/styles";

const useStyles = makeStyles((theme) => ({
  drawer: {
    flexShrink: 0,

    "& .MuiDrawer-paper": {
      width: theme.components.drawerWidth,
      top: theme.components.appBarMinHeight,
      boxSizing: "border-box",
    },
    backgroundColor: theme.palette.background.paper,
    color: theme.palette.primary.main,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },

  accordion: {
    padding: "10px 20px",
    "&:hover": {
      color: theme.palette.primary.main,
    },
  },
  accordionOpen: {
    padding: "10px 20px",
    color: theme.palette.primary.main,
  },
  headerTypography: {
    fontWeight: 400,

    color: "inherit",
    width: "80%",
    flexShrink: 0,
  },
  summaryIcon: {
    color: theme.palette.primary.main,
    marginRight: ".5rem",
  },

  link: {
    color: theme.palette.text.secondary,
    textDecoration: "none",
    fontSize: ".9rem",
    padding: "8px",

    "&:hover": {
      color: theme.palette.primary.main,
    },
  },
  activeLink: {
    color: theme.palette.primary.main,
    textDecoration: "none",
    fontSize: ".9rem",
    padding: "8px",
  },
}));

////////////////////////////// Custom links //////////////////////////////
const CustomLink = (props) => {
  const location = useLocation();
  const classes = useStyles();
  const { to, label } = props;

  return (
    <Link
      to={to}
      className={location.pathname === to ? classes.activeLink : classes.link}
    >
      <Stack direction={"row"}>
        <ArrowRightOutlinedIcon />
        <Typography variant="body2">{label}</Typography>
      </Stack>
    </Link>
  );
};

const SimpleLink = (props) => {
  const { to, label, icon } = props;
  const location = useLocation();
  const classes = useStyles();

  return (
    <Link to={to} className={classes.link} style={{ margin: 0, padding: 0 }}>
      <Accordion
        disableGutters
        elevation={0}
        square
        className={location === to ? classes.accordionOpen : classes.accordion}
      >
        <AccordionSummary>
          {icon}
          <Typography className={classes.headerTypography}>{label}</Typography>
        </AccordionSummary>
      </Accordion>
    </Link>
  );
};

const AccordionLink = (props) => {
  const { menuItem, index, state } = props;
  const { expanded, setExpanded } = state;
  const classes = useStyles();

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <Accordion
      expanded={expanded === `panel${index}`}
      onChange={handleChange(`panel${index}`)}
      disableGutters
      elevation={0}
      square
      className={
        expanded === `panel${index}` ? classes.accordionOpen : classes.accordion
      }
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon style={{ fontSize: "1rem" }} />}
        aria-controls={`panel${index}bh-content`}
        id={`panel${index}bh-header`}
        disableGutters
        elevation={0}
      >
        {menuItem.icon}
        <Typography className={classes.headerTypography}>
          {menuItem.label}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Stack direction={"column"} className={classes.linkStack}>
          {menuItem.links.map((link, index) => (
            <CustomLink key={index} to={link.to} label={link.label} />
          ))}
        </Stack>
      </AccordionDetails>
    </Accordion>
  );
};

////////////////////////////// Custom drawer //////////////////////////////
const ResponsiveDrawer = (props) => {
  const { open, setOpen, children } = props;
  const classes = useStyles();
  const theme = useTheme();
  const isMediumBp = useMediaQuery(theme.breakpoints.down("md"));

  useEffect(() => {
    if (isMediumBp) return setOpen(false);
    return setOpen(true);
  }, [isMediumBp]);
  return (
    <Drawer
      style={open ? { width: theme.components.drawerWidth } : { width: 0 }}
      className={classes.drawer}
      variant={isMediumBp ? "temporary" : "persistent"}
      anchor="left"
      open={open}
      hideBackdrop
    >
      {children}
    </Drawer>
  );
};

export default function Navigation({ open, setOpen }) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  ////////////////////////////// Menu items object //////////////////////////////
  const menuItems = [
    {
      label: "Comenzi",
      icon: <ShoppingCartOutlinedIcon className={classes.summaryIcon} />,
      links: [
        {
          label: "Adauga comanda",
          to: "/adauga-comanda",
        },
        {
          label: "Cauta comanda",
          to: "/lista-comenzi",
        },
      ],
    },
    {
      label: "Garnije",
      icon: <BlurOnOutlinedIcon className={classes.summaryIcon} />,
      links: [
        {
          label: "Adauga garnije",
          to: "/garnije/adauga",
        },
        {
          label: "Adauga suporturi bara",
          to: "/garnije/suporturi-bara/adauga",
        },
        {
          label: "Adauga consola bara",
          to: "/garnije/consola-bara/adauga",
        },
        {
          label: "Adauga capete de bara",
          to: "/garnije/capete-bara/adauga",
        },
        {
          label: "Adauga inele",
          to: "/garnije/inele/adauga",
        },
        {
          label: "Adauga accesorii unghiulare",
          to: "/garnije/acc-unghiulare/adauga",
        },
        {
          label: "Lista garnije",
          to: "/garnije/lista",
        },
      ],
    },
    {
      label: "Perdele",
      icon: <BlurOnOutlinedIcon className={classes.summaryIcon} />,
      links: [
        {
          label: "Adauga perdele",
          to: "/perdele/adauga",
        },
        {
          label: "Adauga rejanse",
          to: "/perdele/rejanse/adauga",
        },
        {
          label: "Adauga accesorii perdele",
          to: "/perdele/acc-perdele/adauga",
        },
        {
          label: "Lista perdele",
          to: "/perdele/lista",
        },
      ],
    },
    {
      label: "Utilizatori",
      icon: <GroupOutlinedIcon className={classes.summaryIcon} />,
      links: [
        {
          label: "Adauga utilizator",
          to: "/utilizatori/adauga",
        },
        {
          label: "Lista utilizatori",
          to: "/utilizatori/lista",
        },
      ],
    },
    {
      label: "Locatie",
      icon: <LocationOnOutlinedIcon className={classes.summaryIcon} />,
      links: [
        {
          label: "Adauga locatie",
          to: "/locatie/adauga",
        },
        {
          label: "Lista locatie",
          to: "/locatie/lista",
        },
      ],
    },
  ];

  return (
    <ResponsiveDrawer open={open} setOpen={setOpen}>
      <SimpleLink
        to="/"
        label="Dashboard"
        icon={<HomeOutlinedIcon className={classes.summaryIcon} />}
      />

      {menuItems.map((item, index) => (
        <AccordionLink
          key={index}
          menuItem={item}
          index={index}
          state={{ expanded, setExpanded }}
        />
      ))}
    </ResponsiveDrawer>
  );
}
