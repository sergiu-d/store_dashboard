import React, { useState } from "react";

// Material UI

import {
  Button,
  AppBar,
  Stack,
  Box,
  Toolbar,
  IconButton,
  Typography,
  useTheme,
} from "@mui/material";
// import { styled, useTheme } from "@mui/material/styles";
import NotificationsNoneOutlinedIcon from "@mui/icons-material/NotificationsNoneOutlined";

import MenuIcon from "@mui/icons-material/Menu";

const AppTopBar = ({ setOpen }) => {
  const theme = useTheme();

  const handleDrawerToggle = () => {
    setOpen((prev) => !prev);
  };
  return (
    <AppBar
      position="sticky"
      sx={{
        backgroundColor: theme.palette.background.paper,
        color: theme.palette.primary.main,
        minHeight: theme.components.appBarMinHeight,
        zIndex: (theme) => theme.zIndex.drawer + 1,
      }}
    >
      <Toolbar>
        <Box sx={{ width: theme.components.drawerWidth }}>
          <Typography variant="h3">LOGO</Typography>
        </Box>

        <Box
          sx={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerToggle}
            edge="start"
            sx={{
              marginLeft: 3,
            }}
          >
            <MenuIcon />
          </IconButton>

          <Stack direction={"row"} sx={{ alignItems: "center" }}>
            <Button>
              <NotificationsNoneOutlinedIcon sx={{ cursor: "pointer" }} />
            </Button>
            <Button variant="text">Utilizator</Button>
          </Stack>
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export default AppTopBar;
