import React, { useState } from "react";
// Router
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Outlet,
} from "react-router-dom";

// Material UI
import { Stack } from "@mui/material";
import { styled, useTheme } from "@mui/material/styles";

// Components
import AppTopBar from "./AppBar";
import Navigation from "./Navigation";

const Main = styled("main", { shouldForwardProp: (prop) => prop !== "open" })(
  ({ theme, open }) => ({
    flexGrow: 1,
    backgroundColor: theme.palette.background.main,
    minHeight: "100vh",
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${theme.components.drawerWidth}px`,
    [theme.breakpoints.down("md")]: {
      marginLeft: 0,
    },
    ...(open && {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),

      marginLeft: 0,
    }),
  })
);

const Layout = () => {
  const [open, setOpen] = useState(true);

  return (
    <Stack direction="column">
      <AppTopBar setOpen={setOpen} />
      <Stack direction="row">
        <Navigation open={open} setOpen={setOpen} />
        <Main open={open}>
          <Outlet />
        </Main>
      </Stack>
    </Stack>
  );
};

export default Layout;
