import React from "react";

import { Routes, Route, Outlet } from "react-router-dom";
// Components //
import Layout from "./layout/Layout";
import Dashboard from "./pages/Dashboard";

// Comanda
import AdaugaComanda from "./pages/comenzi/AdaugaComanda";

// Garnije
import Garnije from "./pages/garnije/Garnije";
import SuporturiBara from "./pages/garnije/SuporturiBara";
import ConsolaBara from "./pages/garnije/ConsolaBara";
import CapeteBara from "./pages/garnije/CapeteBara";
import Inele from "./pages/garnije/Inele";
import AccUnghiulare from "./pages/garnije/AccUnghiulare";

// Perdele
import Perdele from "./pages/perdele/Perdele";
import Rejanse from "./pages/perdele/Rejanse";
import AccPerdele from "./pages/perdele/AccPerdele";

// Utilizatori
import Ustilizatori from "./pages/utilizatori/Utilizatori";

// Locatie
import Locatie from "./pages/locatie/Locatie";

// Table
import AccessoriesTable from "./components/tables/AccessoriesTable";

// Material Ui //
import { ThemeProvider } from "@mui/material/styles";
import { theme } from "./theme/theme";
import CssBaseline from "@mui/material/CssBaseline";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Dashboard />} />
            <Route path="adauga-comanda" element={<AdaugaComanda />} />
            <Route path="lista-comenzi" element={<h1>Lista comenzi</h1>} />
            {/* Garnije */}
            <Route path="garnije">
              <Route path="adauga" element={<Garnije />} />
              <Route path="suporturi-bara/adauga" element={<SuporturiBara />} />
              <Route path="consola-bara/adauga" element={<ConsolaBara />} />
              <Route path="capete-bara/adauga" element={<CapeteBara />} />
              <Route path="inele/adauga" element={<Inele />} />
              <Route path="acc-unghiulare/adauga" element={<AccUnghiulare />} />
              <Route path="lista" element={<AccessoriesTable />} />
            </Route>

            {/* Perdele */}
            <Route path="perdele">
              <Route path="adauga" element={<Perdele />} />
              <Route path="rejanse/adauga" element={<Rejanse />} />
              <Route path="acc-perdele/adauga" element={<AccPerdele />} />

              <Route path="lista" element={<h1>Perdele Lista</h1>} />
            </Route>

            {/* Utilizatori */}
            <Route path="utilizatori">
              <Route path="adauga" element={<Ustilizatori />} />
              <Route path="lista" element={<h1>Utilizatori Lista</h1>} />
            </Route>

            {/* Locatie */}
            <Route path="locatie">
              <Route path="adauga" element={<Locatie />} />
              <Route path="lista" element={<h1>Locatii Lista</h1>} />
            </Route>
          </Route>
        </Routes>
      </CssBaseline>
    </ThemeProvider>
  );
}

export default App;
