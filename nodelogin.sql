-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 16, 2022 at 11:51 AM
-- Server version: 8.0.28
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nodelogin`
--

-- --------------------------------------------------------

--
-- Table structure for table `accessories`
--

CREATE TABLE `accessories` (
  `id` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `price` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `accessories`
--

INSERT INTO `accessories` (`id`, `name`, `color`, `price`) VALUES
(9, 'produs 2', 'verde', 34),
(11, 'produs10', 'negru', 40);

-- --------------------------------------------------------

--
-- Table structure for table `acc_angular`
--

CREATE TABLE `acc_angular` (
  `id` int NOT NULL,
  `shape` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `width` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `color` varchar(100) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `acc_angular`
--

INSERT INTO `acc_angular` (`id`, `shape`, `width`, `color`) VALUES
(1, 'rotunde', '25', 'inox'),
(2, 'rotunde', '25', 'cupru antic');

-- --------------------------------------------------------

--
-- Table structure for table `acc_curtains`
--

CREATE TABLE `acc_curtains` (
  `id` int NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `color` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `acc_curtains`
--

INSERT INTO `acc_curtains` (`id`, `name`, `color`, `price`) VALUES
(1, 'garnij 3', 'maro', '233.00');

-- --------------------------------------------------------

--
-- Table structure for table `acc_poles`
--

CREATE TABLE `acc_poles` (
  `id` int NOT NULL,
  `collection` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `shape` varchar(100) NOT NULL,
  `width` varchar(5) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `acc_poles`
--

INSERT INTO `acc_poles` (`id`, `collection`, `type`, `shape`, `width`, `price`) VALUES
(18, 'perete', 'simplu', 'rotund', '', '2340.00'),
(22, 'perete', 'simplu', 'rotund', '', '20.00'),
(23, 'tavan', 'dublu', 'patrat', '20/20', '0.00'),
(24, 'perete', 'simplu', 'rotund', '19', '0.00'),
(25, 'perete', 'simplu', 'rotund', '', '58.00'),
(26, 'perete', 'simplu', 'rotund', '19', '0.00'),
(27, 'perete', 'simplu', 'rotund', '19', '0.00'),
(28, 'perete', 'simplu', 'rotund', '19', '0.00'),
(29, 'perete', 'dublu', 'rotund', '19/25', '0.00'),
(30, 'perete', 'simplu', 'rotund', '', '38.00'),
(31, 'perete', 'dublu', 'rotund', '19/25', '0.00'),
(32, 'perete', 'dublu', 'rotund', '19/25', '0.00'),
(33, 'perete', 'dublu', 'rotund', '19/25', '0.00'),
(34, 'perete', 'dublu', 'rotund', '19/25', '0.00'),
(35, 'tavan', 'dublu', 'rotund', '16/16', '0.00'),
(36, 'perete', 'simplu', 'rotund', '25', '456.00');

-- --------------------------------------------------------

--
-- Table structure for table `acc_polestemp`
--

CREATE TABLE `acc_polestemp` (
  `id` int NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `color` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `acc_polestemp`
--

INSERT INTO `acc_polestemp` (`id`, `name`, `color`, `price`) VALUES
(1, 'perdea4', 'rosu', '12.23');

-- --------------------------------------------------------

--
-- Table structure for table `curtains`
--

CREATE TABLE `curtains` (
  `id` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` int NOT NULL,
  `variant` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `curtains`
--

INSERT INTO `curtains` (`id`, `name`, `code`, `variant`, `color`, `price`) VALUES
(2, 'perdea2', 123, 'simplu', 'maro', '50.00'),
(4, 'perdea4', 1234, 'simplu', 'verde', '40.00');

-- --------------------------------------------------------

--
-- Table structure for table `curtain_poles`
--

CREATE TABLE `curtain_poles` (
  `id` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `curtain_poles`
--

INSERT INTO `curtain_poles` (`id`, `name`, `color`, `price`) VALUES
(1, 'garnij137', 'negru', '50.00');

-- --------------------------------------------------------

--
-- Table structure for table `grosgrains`
--

CREATE TABLE `grosgrains` (
  `id` int NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `width` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `grosgrains`
--

INSERT INTO `grosgrains` (`id`, `name`, `width`, `model`, `price`) VALUES
(1, 'mbai', '10', 'neutru', '12.23');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int NOT NULL,
  `city` varchar(100) NOT NULL,
  `number` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `city`, `number`) VALUES
(1, 'Bucuresti', 2);

-- --------------------------------------------------------

--
-- Table structure for table `poles`
--

CREATE TABLE `poles` (
  `id` int NOT NULL,
  `collection` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `thickness` int NOT NULL,
  `shape` varchar(100) NOT NULL,
  `width` decimal(3,2) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `poles`
--

INSERT INTO `poles` (`id`, `collection`, `color`, `thickness`, `shape`, `width`, `price`) VALUES
(19, 'bara134', 'verde', 16, 'patrat', '2.00', '1554.00'),
(22, 'bara134', 'verde', 19, 'patrat', '1.60', '233.00'),
(23, 'bara134', 'verde', 19, 'patrat', '2.40', '233.00'),
(24, 'bara1113', 'verde', 19, 'patrat', '2.40', '123.00'),
(25, 'bara13', 'verde', 19, 'rotund', '2.00', '12.00'),
(26, 'bara134', 'gri', 19, 'rotund', '2.40', '12.00'),
(27, 'bara55', 'negru', 19, 'nespecificat', '2.40', '12.00');

-- --------------------------------------------------------

--
-- Table structure for table `pole_rings`
--

CREATE TABLE `pole_rings` (
  `id` int NOT NULL,
  `dimension` int NOT NULL,
  `color` varchar(100) NOT NULL,
  `shape` varchar(100) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `pole_rings`
--

INSERT INTO `pole_rings` (`id`, `dimension`, `color`, `shape`, `price`) VALUES
(14, 19, 'verde', 'rotund', '102.00'),
(18, 19, 'gri', 'rotund', '12.00'),
(19, 19, 'negru', 'rotund', '233.00'),
(20, 19, 'negru', 'rotund', '233.00'),
(21, 19, 'negru', 'rotund', '233.00'),
(22, 19, 'verde', 'rotund', '456.00'),
(24, 25, 'gri', 'rotund', '123.00'),
(25, 25, 'gri', 'rotund', '12.00'),
(26, 19, 'verde', 'rotund', '233.00'),
(27, 25, 'gri', 'rotund', '12.00'),
(28, 25, 'gri', 'rotund', '12.00'),
(29, 30, 'verde', 'rotund', '123.00'),
(30, 25, 'maro', 'rotund', '233.00');

-- --------------------------------------------------------

--
-- Table structure for table `pole_sides`
--

CREATE TABLE `pole_sides` (
  `id` int NOT NULL,
  `name` varchar(100) NOT NULL,
  `dimension` int NOT NULL,
  `shape` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `pole_sides`
--

INSERT INTO `pole_sides` (`id`, `name`, `dimension`, `shape`, `color`, `price`) VALUES
(7, 'transparent', 19, 'patrat', 'maro', '40.00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` int NOT NULL,
  `location` int NOT NULL,
  `user_type` int NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `phone`, `location`, `user_type`, `email`) VALUES
(11, 'super admin', '$2a$08$yYCPzY01f9vPWg6/oxigJu/fiCH0q6p5h0638Kj/Hv35/iUJHh3HO', 'super', 'admin', 345345453, 1, 1, 'superadmin@superadmin.com'),
(15, 'admin2', '$2a$08$P4J4Q9mgLqm5olMuYwrMP.JQPqtfv7U83XZ0/QGVr27inIs6r2vKi', 'user   12    ', '15         ', 3534, 2, 3, ''),
(44, 'vanz19', '$2a$08$fdp2p3QxqGJpJJ.L0COzlOA4AHTz.tSWo8Y2F3n1qP3KIkDs/QU7K', 'vanz', '19', 346, 2, 1, ''),
(45, 'adminus', '$2a$08$Sam0rViK944lLZjbc31E3eJ9UiuV37ofE9G0dkk3o1t5nvd0qLaKq', 'sfsdf', 'sadfsad', 91836526, 2, 1, ''),
(47, 'adminus3', '$2a$08$pSy1Cqa8QJozx8hoK.Cb0OEbNVIzA9Zl9VT2BRmx4RN7AzY5iE1qy', 'admin', 'sdsfd', 234243, 2, 1, 'g@g.c'),
(48, 'tester', '$2a$08$bv7dR76dzs/7lepgK2/JS.Qsvx.ihD/QyHpahreia/p5qw4qaNKge', '34desdfsfad', 'test', 4543, 1, 1, 'test@test.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accessories`
--
ALTER TABLE `accessories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acc_angular`
--
ALTER TABLE `acc_angular`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acc_curtains`
--
ALTER TABLE `acc_curtains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acc_poles`
--
ALTER TABLE `acc_poles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acc_polestemp`
--
ALTER TABLE `acc_polestemp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curtains`
--
ALTER TABLE `curtains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `curtain_poles`
--
ALTER TABLE `curtain_poles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grosgrains`
--
ALTER TABLE `grosgrains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poles`
--
ALTER TABLE `poles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pole_rings`
--
ALTER TABLE `pole_rings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pole_sides`
--
ALTER TABLE `pole_sides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accessories`
--
ALTER TABLE `accessories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `acc_angular`
--
ALTER TABLE `acc_angular`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `acc_curtains`
--
ALTER TABLE `acc_curtains`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `acc_poles`
--
ALTER TABLE `acc_poles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `acc_polestemp`
--
ALTER TABLE `acc_polestemp`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `curtains`
--
ALTER TABLE `curtains`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `curtain_poles`
--
ALTER TABLE `curtain_poles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `grosgrains`
--
ALTER TABLE `grosgrains`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `poles`
--
ALTER TABLE `poles`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `pole_rings`
--
ALTER TABLE `pole_rings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pole_sides`
--
ALTER TABLE `pole_sides`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
