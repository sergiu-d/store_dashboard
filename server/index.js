const express = require("express");
const app = express();
const { requestConnectionDB, db } = require("./db/config");
const cors = require("cors");
require("dotenv").config();

app.use(cors());
app.use(express.json());

const api = {
  name: "test",
  age: "test",
};

app.use(
  "/api",
  requestConnectionDB,
  // authenticateToken,
  require("./routes/api")
);

const PORT = process.env.PORT || 4500;
app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
