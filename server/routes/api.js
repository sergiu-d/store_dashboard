const express = require("express");
const router = express.Router();

const {
  // User
  getUserInfo,
  // Poles
  getPoles,
  getAccPoles,
  getAccPolesTemp,
  getPolesSides,
  getPolesRings,
  getAccAngular,
  // Curtains
  getCurtains,
  getGrosgrains,
  getAccCurtains,
  ///
  pagination,
} = require("../controllers/api");

////////////////////// User API //////////////////////
router.get("/user", getUserInfo);

////////////////////// Products APIs //////////////////////

// Poles APIs
router.get("/poles", getPoles);
router.get("/acc_poles", getAccPoles);
router.get("/acc_polesTemp", getAccPolesTemp);
router.get("/pole_sides", getPolesSides);
router.get("/pole_rings", getPolesRings);
router.get("/acc_angular", getAccAngular);

// Curtains APIs
router.get("/curtains", getCurtains);
router.get("/grosgrains", getGrosgrains);
router.get("/acc_curtains", getAccCurtains);

///
router.get("/pagination", pagination);

module.exports = router;
