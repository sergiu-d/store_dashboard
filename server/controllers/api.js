const { db } = require("../db/config");

const userType = {
  1: "Admin",
  2: "Vanzator",
  3: "Admin punct de lucru",
  4: "Vanzator punct de lucru",
};

////////////////////// User //////////////////////

exports.getUserInfo = async (req, res) => {
  const { userId } = req;
  try {
    const query = `SELECT username, first_name, last_name, location, email, user_type FROM users WHERE id = ?`;
    const [user] = await db.query(query, [userId]);

    user[0].user_type = userType[user[0].user_type];
    res.json(user);
  } catch (err) {
    console.log(err);
  }
};

////////////////////// Products //////////////////////

// Poles
exports.getPoles = async (req, res) => {
  try {
    const query = `SELECT * FROM poles`;
    const [poles] = await db.query(query);

    res.json(poles);
  } catch (err) {
    console.log(err);
  }
};

exports.getAccPoles = async (req, res) => {
  try {
    const query = `SELECT * FROM acc_poles`;
    const [acc_poles] = await db.query(query);

    res.json(acc_poles);
  } catch (err) {
    console.log(err);
  }
};
exports.getAccPolesTemp = async (req, res) => {
  try {
    const query = `SELECT * FROM acc_polesTemp`;
    const [acc_polesTemp] = await db.query(query);

    res.json(acc_polesTemp);
  } catch (err) {
    console.log(err);
  }
};
exports.getPolesSides = async (req, res) => {
  try {
    const query = `SELECT * FROM pole_sides`;
    const [pole_sides] = await db.query(query);

    res.json(pole_sides);
  } catch (err) {
    console.log(err);
  }
};
exports.getPolesRings = async (req, res) => {
  try {
    const query = `SELECT * FROM pole_rings`;
    const [pole_rings] = await db.query(query);

    res.json(pole_rings);
  } catch (err) {
    console.log(err);
  }
};
exports.getAccAngular = async (req, res) => {
  try {
    const query = `SELECT * FROM acc_angular`;
    const [acc_angular] = await db.query(query);

    res.json(acc_angular);
  } catch (err) {
    console.log(err);
  }
};

// Curtains
exports.getCurtains = async (req, res) => {
  try {
    const query = `SELECT * FROM curtains`;
    const [curtains] = await db.query(query);

    res.json(curtains);
  } catch (err) {
    console.log(err);
  }
};
exports.getGrosgrains = async (req, res) => {
  try {
    const query = `SELECT * FROM grosgrains`;
    const [grosgrains] = await db.query(query);

    res.json(grosgrains);
  } catch (err) {
    console.log(err);
  }
};
exports.getAccCurtains = async (req, res) => {
  try {
    const query = `SELECT * FROM acc_curtains`;
    const [acc_curtains] = await db.query(query);

    res.json(acc_curtains);
  } catch (err) {
    console.log(err);
  }
};

////////////////////// Pagination //////////////////////
exports.pagination = async (req, res) => {
  const { limit, offSet, orderBy } = req.query;
  const limitInt = parseInt(limit);
  const offSetInt = parseInt(offSet);

  try {
    const query = `SELECT * FROM acc_poles ORDER BY ? LIMIT ?, ?`;
    const [products] = await db.query(query, [orderBy, limitInt, offSetInt]);

    res.json(products);
  } catch (err) {
    console.log(err);
  }
};
