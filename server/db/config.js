const mysql = require("mysql2");
const dotenv = require("dotenv");

dotenv.config({ path: "./.env" });

const pool = mysql.createConnection({
  multipleStatements: true,
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
});

const db = pool.promise();

function requestConnectionDB(req, res, next) {
  pool.connect((err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("MYSQL Connected...");
    }
  });
  next();
}

module.exports = { requestConnectionDB, db };
